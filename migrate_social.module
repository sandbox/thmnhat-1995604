<?php

/**
 * @file
 * Migrate Social
 */

$module_path = dirname(__FILE__);

include_once 'migrate_social.inc';
include_once $module_path . '/includes/facebook/facebook.inc';
include_once $module_path . '/includes/twitter/twitter.inc';

/**
 * Implements hook_menu().
 */
function migrate_social_menu() {
  $items = array();

  if (module_exists('migrate_ui')) {
    $items['admin/content/migrate/social'] = array(
      'title'            => 'Social Networks',
      'page callback'    => 'drupal_get_form',
      'page arguments'   => array('migrate_social_settings'),
      'access arguments' => array('administer migrate social'),
      'type'             => MENU_LOCAL_TASK,
      'file'             => 'migrate_social.admin.inc',
    );
  }

  return $items;
}

/**
 * Implements hook_permission().
 */
function migrate_social_permission() {
  $perms = array();

  if (module_exists('migrate_ui')) {
    $perms['administer migrate social'] = array(
      'title'       => t('Administer Migrate Social'),
      'description' => t('Social Network settings'),
    );
  }

  return $perms;
}

/**
 * Implements hook_libraries_info().
 */
function migrate_social_libraries_info() {
  if (!module_exists('fbconnect')) {
    // Returns an associative array, with information about external library.
    $libraries['facebook-php-sdk'] = array(
      'name'              => 'Facebook PHP SDK',
      'vendor url'        => 'https://github.com/facebook/facebook-php-sdk',
      'download url'      => 'https://github.com/facebook/facebook-php-sdk/tarball/v3.2.2',
      'version arguments' => array(
        'file'    => 'readme.md',
        'pattern' => '/Facebook PHP SDK \(v\.(3\.\d\.\d)\)/',
        'lines'   => 1,
      ),
      // Supported library version, including relevant files.
      'versions' => array(
        '3.2.2' => array(
          'files' => array(
            'php' => array(
              'src/base_facebook.php',
              'src/facebook.php',
            ),
          ),
        ),
      ),
    );
  }

  return $libraries;
}

/**
 * Implements hook_migrate_social_networks().
 */
function migrate_social_migrate_social_networks() {
  return array(
    'facebook' => array(
      'label'             => 'Facebook',
      'class'             => 'FacebookMigrate',
      'settings callback' => 'migrate_social_facebook_settings',
      'settings submit'   => 'migrate_social_facebook_settings_submit',
    ),
    'twitter' => array(
      'label'             => 'Twitter',
      'class'             => 'TwitterMigrate',
      'settings callback' => 'migrate_social_twitter_settings',
      'settings submit'   => 'migrate_social_twitter_settings_submit',
    ),
  );
}

/**
 * Implements hook_theme().
 */
function migrate_social_theme() {
  $module_path = drupal_get_path('module', 'migrate_social');
  $file        = array('file' => 'theme.inc');
  $path        = array('path' => $module_path . '/templates');
  $themes      = array();

  $themes['migrate_social_manage_accounts'] = $file + $path + array(
    'render element' => 'element',
    'pattern'        => 'migrate_social_manage_accounts__',
    'template'       => 'migrate-social-manage-accounts',
  );

  return $themes;
}
