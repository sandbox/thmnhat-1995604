<?php

/**
 * @file
 * Facebook
 */

/**
 * Facebook settings form.
 * @see migrate_social_facebook_settings_account_add()
 * @see migrate_social_facebook_settings_account_remove()
 * @see migrate_social_facebook_settings_callback_account_add()
 * @see migrate_social_facebook_settings_callback_account_remove()
 * @see migrate_social_facebook_settings_submit()
 *
 * @param array $form_state
 *   Form state
 *
 * @return array
 *   Form element
 */
function migrate_social_facebook_settings(&$form_state) {
  $form_state['storage']['test'] = 1;
  $form['migrate_social_facebook_appid'] = array(
    '#type'          => 'textfield',
    '#title'         => t('App ID'),
    '#description'   => t('Your Facebook App ID. Get it <a href="!link" target="_blank">here</a>', array('!link' => 'https://developers.facebook.com')),
    '#default_value' => variable_get('migrate_social_facebook_appid', ''),
  );

  $form['migrate_social_facebook_secret'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Secret'),
    '#description'   => t('Your Facebook Secret'),
    '#default_value' => variable_get('migrate_social_facebook_secret', ''),
  );

  $form['migrate_social_facebook_accounts'] = array(
    '#type'    => 'item',
    '#tree'    => TRUE,
    '#theme'   => 'migrate_social_manage_accounts__facebook',
    '#network' => 'facebook',
    '#prefix'  => '<div id="migrate-social-facebook-accounts">',
    '#suffix'  => '</div>',
  );

  $accounts = variable_get('migrate_social_facebook_accounts', array());
  $idx      = 0;
  $weights  = drupal_map_assoc(range(-50, 50));
  $nums     = empty($form_state['storage']['migrate_social_facebook_accounts']) ? 0 : $form_state['storage']['migrate_social_facebook_accounts'];

  foreach ($accounts as $account) {
    $element = array(
      '#type' => 'item',
      '#tree' => TRUE,
    );

    $element['id'] = array(
      '#type'  => 'value',
      '#value' => $account['id'],
    );

    $element['label'] = array(
      '#markup' => l($account['id'], 'https://facebook.com/' . $account['id'], array('attributes' => array('target' => '_blank'))),
    );

    $element['delete'] = array(
      '#type' => 'hidden',
    );

    $element['status'] = array(
      '#type'          => 'checkbox',
      '#title'         => t('Status'),
      '#title_display' => 'invisible',
      '#return_value'  => 1,
      '#default_value' => !empty($account['status']),
    );

    $element['weight'] = array(
      '#type'          => 'select',
      '#title'         => t('Weight'),
      '#title_display' => 'invisible',
      '#options'       => $weights,
      '#attributes'    => array(
        'class' => array('facebook-account-weight'),
      ),
    );

    $element['action'] = array(
      '#type'   => 'submit',
      '#name'   => 'facebook_account_remove_' . $idx,
      '#value'  => t('Remove'),
      '#submit' => array('migrate_social_facebook_settings_account_remove'),
      '#index'  => $idx,
      '#ajax'   => array(
        'callback' => 'migrate_social_facebook_settings_callback_account_remove',
        'progress' => array('type' => 'throbber', 'message' => ''),
        'wrapper'  => 'migrate-social-facebook-accounts',
      ),
    );

    $form['migrate_social_facebook_accounts'][$idx] = $element;
    ++$idx;
  }

  $empty = array(
    '#type'  => 'item',
    '#tree'  => TRUE,
  );

  for (; $idx < $nums; ++$idx) {
    $element = $empty;

    $element['id'] = array(
      '#type'          => 'textfield',
      '#title'         => 'Account ID',
      '#title_display' => 'invisible',
      '#attributes'    => array(
        'style' => array('width: auto'),
      ),
    );

    $element['delete'] = array(
      '#type' => 'hidden',
    );

    $element['status'] = array(
      '#type'          => 'checkbox',
      '#title'         => t('Status'),
      '#title_display' => 'invisible',
      '#return_value'  => 1,
    );

    $element['weight'] = array(
      '#type'          => 'select',
      '#title'         => t('Weight'),
      '#title_display' => 'invisible',
      '#options'       => $weights,
      '#attributes'    => array(
        'class' => array('facebook-account-weight'),
      ),
    );

    $element['action'] = array(
      '#type'   => 'submit',
      '#name'   => 'facebook_account_remove_' . $idx,
      '#value'  => t('Remove'),
      '#submit' => array('migrate_social_facebook_settings_account_remove'),
      '#index'  => $idx,
      '#ajax'   => array(
        'callback' => 'migrate_social_facebook_settings_callback_account_remove',
        'progress' => array('type' => 'throbber', 'message' => ''),
        'wrapper'  => 'migrate-social-facebook-accounts',
      ),
    );

    $form['migrate_social_facebook_accounts'][$idx] = $element;
  }

  // Last row.
  $element = $empty;

  $element['id'] = array(
    '#type'       => 'textfield',
    '#title'      => 'Add new account',
    '#attributes' => array(
      'style' => array('width: auto'),
    ),
  );

  $element['status'] = array(
    '#type'          => 'hidden',
    '#default_value' => 1,
  );

  $element['weight'] = array(
    '#type'          => 'hidden',
    '#default_value' => 50,
  );

  $element['action'] = array(
    '#type'   => 'submit',
    '#name'   => 'facebook_account_add',
    '#value'  => t('Add'),
    '#submit' => array('migrate_social_facebook_settings_account_add'),
    '#index'  => $idx,
    '#ajax'   => array(
      'callback' => 'migrate_social_facebook_settings_callback_account_add',
      'progress' => array('type' => 'throbber', 'message' => ''),
      'wrapper'  => 'migrate-social-facebook-accounts',
    ),
  );

  $form['migrate_social_facebook_accounts'][$idx] = $element;
  $form_state['storage']['migrate_social_facebook_accounts'] = $idx;

  $form['actions'] = array(
    '#type' => 'actions',
  );

  $form['actions']['reset'] = array(
    '#type'   => 'submit',
    '#name'   => 'facebook_reset',
    '#value'  => t('Remove all crawled posts'),
    '#submit' => array('migrate_social_facebook_settings_reset'),
  );

  return $form;
}

/**
 * Submit handler for adding a new account.
 * @see migrate_social_facebook_settings()
 * @see migrate_social_facebook_settings_callback_account_add()
 *
 * @param array $form
 *   Form element
 *
 * @param array $form_state
 *   Form state
 */
function migrate_social_facebook_settings_account_add($form, &$form_state) {
  $accounts = empty($form_state['values']['migrate_social_facebook_accounts']) ? array() : $form_state['values']['migrate_social_facebook_accounts'];

  if (!empty($accounts)) {
    $account    = array_pop($accounts);
    $account_id = empty($account['id']) ? NULL : trim($account['id']);

    if ($account_id) {
      $form_state['rebuild'] = TRUE;
      $form_state['storage']['migrate_social_facebook_accounts']++;
    }
  }
}

/**
 * Submit handler for removing an account.
 * @see migrate_social_facebook_settings()
 * @see migrate_social_facebook_settings_callback_account_remove()
 *
 * @param array $form
 *   Form element
 *
 * @param array $form_state
 *   Form state
 */
function migrate_social_facebook_settings_account_remove(&$form, &$form_state) {
  $trigger = $form_state['triggering_element'];
  $index   = $trigger['#index'];
  $element = &$form['facebook']['migrate_social_facebook_accounts'][$index];

  $form_state['rebuild'] = TRUE;
  form_set_value($element['delete'], 1, $form_state);
  drupal_array_set_nested_value($form_state['input'], $element['delete']['#parents'], 1);
}

/**
 * Facebook settings form submit handler.
 * @see migrate_social_facebook_settings()
 *
 * @param array $form
 *   Form element
 *
 * @param array $form_state
 *   Form state
 */
function migrate_social_facebook_settings_submit($form, &$form_state) {
  $accounts = empty($form_state['values']['migrate_social_facebook_accounts']) ? array() : $form_state['values']['migrate_social_facebook_accounts'];

  if (!empty($accounts)) {
    array_pop($accounts);
    $last_weight   = 0;
    $last_interval = 0;

    foreach ($accounts as $key => $account) {
      if (!empty($account['delete'])) {
        unset($accounts[$key]);
        continue;
      }

      if ($account['weight'] == $last_weight) {
        $accounts[$key]['weight'] = $last_weight + $last_interval;
        $last_interval += 0.1;
      }
      else {
        $last_weight   = $account['weight'];
        $last_interval = 0;
      }
    }

    uasort($accounts, 'drupal_sort_weight');

    $weight = 0;

    foreach ($accounts as $key => $account) {
      $accounts[$key]['weight'] = $weight++;
    }

    variable_set('migrate_social_facebook_accounts', array_values($accounts));
  }
}

/**
 * Facebook form callback for adding a new account.
 * @see migrate_social_facebook_settings()
 * @see migrate_social_facebook_settings_account_add()
 *
 * @param array $form_state
 *   Form state
 *
 * @return array
 *   Form element
 */
function migrate_social_facebook_settings_callback_account_add($form, &$form_state) {
  return $form['facebook']['migrate_social_facebook_accounts'];
}

/**
 * Facebook form callback for removing an account.
 * @see migrate_social_facebook_settings()
 * @see migrate_social_facebook_settings_account_remove()
 *
 * @param array $form_state
 *   Form state
 *
 * @return array
 *   Form element
 */
function migrate_social_facebook_settings_callback_account_remove($form, &$form_state) {
  return $form['facebook']['migrate_social_facebook_accounts'];
}
