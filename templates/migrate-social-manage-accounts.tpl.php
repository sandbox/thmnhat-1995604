<?php

/**
 * @file
 * Available variables:
 * - $last_row: The last row.
 * - $rows: An array of rows.
 * - $form_submit: Form submit button.
 *
 * @ingroup themeable
 */
?>
<table id="<?php echo $table_id ?>" class="sticky-enabled">
  <thead>
    <tr>
      <th><?php echo t('Account ID'); ?></th>
      <th><?php echo t('Enabled'); ?></th>
      <th><?php echo t('Weight'); ?></th>
      <th><?php echo t('Operations'); ?></th>
    </tr>
  </thead>
  <tbody>
    <?php if(!empty($rows)) : ?>
    <tr class="region-title">
      <td colspan="4"><b><?php echo t('Accounts'); ?></b></td>
    </tr>
      <?php $i = 0; ?>
      <?php foreach ($rows as $row) : ?>
      <tr class="draggable <?php echo $i & 1 ? 'odd' : 'even'; ?>">
        <td><?php echo $row['id']; ?></td>
        <td><?php echo $row['status']; ?></td>
        <td><?php echo $row['weight']; ?></td>
        <td><?php echo $row['action']; ?></td>
      </tr>
        <?php ++$i; ?>
      <?php endforeach; ?>
    <?php endif; ?>
    <tr>
      <td colspan="2">
        <?php echo $last_row['id']; ?>
        <?php echo $last_row['status']; ?>
      </td>
      <td><?php echo $last_row['weight']; ?></td>
      <td><?php echo $last_row['action']; ?></td>
    </tr>
  </tbody>
</table>

<div class="element-invisible">
  <?php echo $hiddens ?>
</div>
