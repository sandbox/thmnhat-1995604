<?php

/**
 * @file
 * Migrate API
 */

/**
 * Implements hook_migrate_api().
 */
function migrate_social_migrate_api() {
  $api = array(
    'api'        => 2,
    'migrations' => array(
      'Facebook' => array('class_name' => 'FacebookMigration'),
      'Twitter'  => array('class_name' => 'TwitterMigration'),
    ),
  );

  return $api;
}
