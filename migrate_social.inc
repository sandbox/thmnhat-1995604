<?php

/**
 * @file
 * Migrate Social
 */

/**
 * Get regisered social networks.
 *
 * @return array
 *   An array contains list of social networks.
 */
function migrate_social_get_networks() {
  $cid   = 'migrate_social:networks';
  $cache = cache_get($cid);

  if ($cache) {
    return $cache->data;
  }

  $networks = array();
  $hook     = 'migrate_social_networks';
  $modules  = module_implements($hook);

  foreach ($modules as $module) {
    $items = call_user_func($module . '_' . $hook);

    if (isset($items) && is_array($items)) {
      foreach (array_keys($items) as $network) {
        $items[$network]['module'] = $module;
      }

      $networks = array_merge($networks, $items);
    }
  }

  drupal_alter($hook, $networks);
  cache_set($cid, $networks);

  return $networks;
}
