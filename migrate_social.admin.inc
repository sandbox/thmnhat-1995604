<?php

/**
 * @file
 * Migrate Social Admin.
 */

/**
 * Admin Settings page.
 */
function migrate_social_settings($form, &$form_state) {
  $form     = array();
  $networks = migrate_social_get_networks();
  $submits  = array();

  $form['settings'] = array(
    '#type'  => 'vertical_tabs',
    '#title' => t('Settings'),
  );

  foreach ($networks as $network => $info) {
    if (empty($info['settings callback']) || !is_callable($info['settings callback'])) {
      continue;
    }

    $elements = call_user_func_array($info['settings callback'], array(&$form_state));

    if (is_array($elements)) {
      $fieldset = array(
        '#type'        => 'fieldset',
        '#title'       => $info['label'],
        '#collapsible' => TRUE,
        '#collapsed'   => FALSE,
        '#group'       => 'settings',
      );

      $form[$network] = $fieldset + $elements;
    }

    if (is_callable($info['settings submit'])) {
      $submits[] = $info['settings submit'];
    }
  }

  $form = system_settings_form($form);
  $form['#submit'] = array_merge($form['#submit'], $submits);

  $form['actions']['submit']['#submit'] = $form['#submit'];
  $form['#submit'] = array();

  return $form;
}
