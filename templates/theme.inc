<?php

/**
 * @file
 * Migrate Social
 */

/**
 * Theme Manage Accounts Table.
 *
 * @param array $variables
 *   An array contains
 *   - element: Form element.
 */
function template_preprocess_migrate_social_manage_accounts(&$variables) {
  $element = &$variables['element'];
  $network = empty($element['#network']) ? NULL : $element['#network'];

  $variables['theme_hook_suggestions'][] = 'migrate_social_manage_accounts';

  if ($network) {
    $variables['theme_hook_suggestions'][] = 'migrate_social_manage_accounts__' . $network;
  }

  $children = element_children($element);
  $rows     = array();
  $last     = array_pop($children);
  $last     = &$element[$last];
  $rows     = array();
  $hiddens  = array();

  foreach ($children as $child) {
    $item   = &$element[$child];
    $delete = empty($item['delete']['#value']) ? FALSE : $item['delete']['#value'];
    $hiddens[] = drupal_render($item['delete']);

    if ($delete) {
      drupal_render($item);
      continue;
    }

    if ($item['id']['#type'] == 'value') {
      $row['id'] = drupal_render($item['label']);
    }
    else {
      $row['id'] = drupal_render($item['id']);
    }

    $row['status'] = drupal_render($item['status']);
    $row['weight'] = drupal_render($item['weight']);
    $row['action'] = drupal_render($item['action']);

    $rows[] = $row;
  }

  $table_id = 'migrate_social_manage_accounts_list_' . $network;

  $variables['table_id'] = $table_id;
  $variables['rows'] = $rows;
  $variables['hiddens'] = implode('', $hiddens);
  $variables['last_row'] = array(
    'id'     => drupal_render($last['id']),
    'status' => drupal_render($last['status']),
    'weight' => drupal_render($last['weight']),
    'action' => drupal_render($last['action']),
  );

  drupal_add_js('misc/tableheader.js');
  drupal_add_tabledrag($table_id, 'order', 'sibling', $network . '-account-weight');
}
